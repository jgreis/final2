# ENTREGA CONVOCATORIA JUNIO 

Joana Gabriela Reis González (jg.reis.2023@alumnos.urjc.es) 

<https://youtu.be/2qzVM0YbR3Y>

Requisitos mínimos cumplidos: 

    - Programa transforms.py  

        - Función Mirror 

        - Función Grayscale 

        - Función Blur 

        - Función Change_colors 

        - Función Rotate 

        - Función Shift 

        - Función Crop 

        - Función Filter 

    - Programa transform_simple.py  

    - Programa transform_args.py  

    - Programa transform_multi.py  