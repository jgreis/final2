# Importamos el módulo images.py que tiene funciones para manipular imágenes.

import images

from PIL import Image

def load_image(filename):
    with Image.open(filename) as img:
        img = img.convert("RGB")
        width, height = img.size
        pixels = list(img.getdata())
        return {
            'width': width,
            'height': height,
            'pixels': pixels
        }

def save_image(image, filename):
    img = Image.new("RGB", (image['width'], image['height']))
    img.putdata(image['pixels'])
    img.save(filename)

def mirror(image: dict) -> dict:
    # Leer las dimensiones de la imagen
    width = image['width']
    height = image['height']
    pixels = image['pixels']

    # Crear una nueva lista de píxeles para la imagen espejada
    mirrored_pixels = pixels.copy()

    # Intercambiar las filas
    for y in range(height // 2):
        # Índice de la fila de arriba
        top_index = y * width
        # Índice de la fila de abajo
        bottom_index = (height - 1 - y) * width

        # Intercambiar las filas
        for x in range(width):
            mirrored_pixels[top_index + x], mirrored_pixels[bottom_index + x] = \
                mirrored_pixels[bottom_index + x], mirrored_pixels[top_index + x]

    # Construir la imagen espejada
    mirrored_image = {
        'width': width,
        'height': height,
        'pixels': mirrored_pixels
    }

    return mirrored_image


def grayscale(image: dict) -> dict:
    # Leer las dimensiones de la imagen
    width = image['width']
    height = image['height']
    pixels = image['pixels']

    # Crear una nueva lista de píxeles para la imagen en escala de grises
    grayscale_pixels = []

    for pixel in pixels:
        # Calcular la media de los valores RGB para obtener el valor gris
        gray_value = sum(pixel) // 3
        # Crear un nuevo píxel en escala de grises (R, G, B iguales)
        grayscale_pixel = (gray_value, gray_value, gray_value)
        grayscale_pixels.append(grayscale_pixel)

    # Construir la imagen en escala de grises
    grayscale_image = {
        'width': width,
        'height': height,
        'pixels': grayscale_pixels
    }

    return grayscale_image


def blur(image: dict) -> dict:
    # Leer las dimensiones de la imagen
    width = image['width']
    height = image['height']
    pixels = image['pixels']

    # Crear una nueva lista de píxeles para la imagen desenfocada
    blurred_pixels = []

    for y in range(height):
        for x in range(width):
            # Lista para acumular los valores RGB de los píxeles vecinos
            neighbors = []

            # Coordenadas de los píxeles vecinos
            coordinates = [
                (y - 1, x),  # arriba
                (y + 1, x),  # abajo
                (y, x - 1),  # izquierda
                (y, x + 1),  # derecha
                (y, x)  # el propio píxel
            ]

            for ny, nx in coordinates:
                if 0 <= ny < height and 0 <= nx < width:
                    neighbors.append(pixels[ny * width + nx])

            # Calcular la media de los valores RGB de los píxeles vecinos
            avg_r = sum(pixel[0] for pixel in neighbors) // len(neighbors)
            avg_g = sum(pixel[1] for pixel in neighbors) // len(neighbors)
            avg_b = sum(pixel[2] for pixel in neighbors) // len(neighbors)

            # Añadir el píxel desenfocado a la lista
            blurred_pixels.append((avg_r, avg_g, avg_b))

    # Construir la imagen desenfocada
    blurred_image = {
        'width': width,
        'height': height,
        'pixels': blurred_pixels
    }

    return blurred_image


def change_colors(image: dict,
                  original: list[tuple[int, int, int]],
                  change: list[tuple[int, int, int]]) -> dict:
    # Leer las dimensiones de la imagen
    width = image['width']
    height = image['height']
    pixels = image['pixels']

    # Crear un diccionario de mapeo de colores
    color_map = {orig: chg for orig, chg in zip(original, change)}

    # Crear una nueva lista de píxeles para la imagen con los colores cambiados
    new_pixels = []

    for pixel in pixels:
        # Cambiar el color del píxel si está en el mapa, de lo contrario dejarlo igual
        new_pixel = color_map.get(pixel, pixel)
        new_pixels.append(new_pixel)

    # Construir la nueva imagen con los colores cambiados
    new_image = {
        'width': width,
        'height': height,
        'pixels': new_pixels
    }

    return new_image


def rotate(image: dict, direction: str) -> dict:
    # Leer las dimensiones de la imagen
    width = image['width']
    height = image['height']
    pixels = image['pixels']

    # Crear una nueva lista de píxeles para la imagen rotada
    new_pixels = [None] * (width * height)

    if direction == 'right':
        # Rotar 90 grados a la derecha
        for y in range(height):
            for x in range(width):
                new_x = height - 1 - y
                new_y = x
                new_pixels[new_y * height + new_x] = pixels[y * width + x]
        new_width = height
        new_height = width
    elif direction == 'left':
        # Rotar 90 grados a la izquierda
        for y in range(height):
            for x in range(width):
                new_x = y
                new_y = width - 1 - x
                new_pixels[new_y * height + new_x] = pixels[y * width + x]
        new_width = height
        new_height = width
    else:
        raise ValueError("Direction must be 'left' or 'right'")

    # Construir la imagen rotada
    rotated_image = {
        'width': new_width,
        'height': new_height,
        'pixels': new_pixels
    }

    return rotated_image


def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    # Leer las dimensiones de la imagen original
    width = image['width']
    height = image['height']
    pixels = image['pixels']

    # Calcular las nuevas dimensiones de la imagen
    new_width = width + horizontal
    new_height = height + vertical

    # Crear una nueva lista de píxeles para la imagen desplazada, inicializada con píxeles negros
    black_pixel = (0, 0, 0)
    new_pixels = [black_pixel] * (new_width * new_height)

    # Copiar los píxeles de la imagen original a las nuevas posiciones en la imagen desplazada
    for y in range(height):
        for x in range(width):
            new_x = x + horizontal
            new_y = y + vertical
            new_pixels[new_y * new_width + new_x] = pixels[y * width + x]

    # Construir la nueva imagen desplazada
    shifted_image = {
        'width': new_width,
        'height': new_height,
        'pixels': new_pixels
    }

    return shifted_image


def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
    # Leer las dimensiones de la imagen original
    original_width = image['width']
    original_height = image['height']
    pixels = image['pixels']

    # Validar las coordenadas y dimensiones del rectángulo de recorte
    if x < 0 or y < 0 or width <= 0 or height <= 0:
        raise ValueError("Las coordenadas y dimensiones del rectángulo deben ser positivas y mayores a cero.")
    if x + width > original_width or y + height > original_height:
        raise ValueError("El rectángulo de recorte se extiende fuera de los límites de la imagen.")

    # Crear una nueva lista de píxeles para la imagen recortada
    cropped_pixels = []

    # Iterar sobre las filas y columnas del rectángulo de recorte
    for row in range(y, y + height):
        for col in range(x, x + width):
            pixel_index = row * original_width + col
            cropped_pixels.append(pixels[pixel_index])

    # Construir la nueva imagen recortada
    cropped_image = {
        'width': width,
        'height': height,
        'pixels': cropped_pixels
    }

    return cropped_image


def filter(image: dict, r: float, g: float, b: float) -> dict:
    # Leer las dimensiones de la imagen
    width = image['width']
    height = image['height']
    pixels = image['pixels']

    # Crear una nueva lista de píxeles para la imagen filtrada
    filtered_pixels = []

    # Iterar sobre cada píxel de la imagen original
    for pixel in pixels:
        # Obtener los componentes RGB del píxel original
        original_r, original_g, original_b = pixel

        # Aplicar el filtro multiplicador a cada componente RGB
        filtered_r = int(original_r * r)
        filtered_g = int(original_g * g)
        filtered_b = int(original_b * b)

        # Asegurarse de que los valores estén dentro del rango válido (0 a 255)
        filtered_r = max(0, min(filtered_r, 255))
        filtered_g = max(0, min(filtered_g, 255))
        filtered_b = max(0, min(filtered_b, 255))

        # Crear el nuevo píxel filtrado y agregarlo a la lista
        filtered_pixels.append((filtered_r, filtered_g, filtered_b))

    # Construir la nueva imagen filtrada
    filtered_image = {
        'width': width,
        'height': height,
        'pixels': filtered_pixels
    }

    return filtered_image


def main():
    # Aquí se pueden añadir pruebas o ejemplos de uso de las funciones

    pass


if __name__ == '__main__':
    main()

