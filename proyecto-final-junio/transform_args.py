import sys
from PIL import Image
import transforms

def parse_color_list(color_list_str):
    colors = []
    for color_str in color_list_str.split(":"):
        r, g, b = map(int, color_str.split(","))
        colors.append((r, g, b))
    return colors

def image_to_dict(img):
    return {
        "width": img.width,
        "height": img.height,
        "pixels": list(img.getdata())
    }

def dict_to_image(img_dict):
    img = Image.new("RGB", (img_dict["width"], img_dict["height"]))
    img.putdata(img_dict["pixels"])
    return img

def main():
    if len(sys.argv) < 3:
        print("Usage: transform_args.py <image_file> <transformation> [<args>...]")
        return

    image_file = sys.argv[1]
    transformation = sys.argv[2]

    img = Image.open(image_file).convert("RGB")
    img_data = image_to_dict(img)

    if transformation == "change_colors":
        if len(sys.argv) < 5:
            print("Usage: transform_args.py <image_file> change_colors <old_colors> <new_colors>")
            return
        old_colors = parse_color_list(sys.argv[3])
        new_colors = parse_color_list(sys.argv[4])
        transformed_data = transforms.change_colors(img_data, old_colors, new_colors)

    elif transformation == "rotate":
        if len(sys.argv) != 4:
            print("Usage: transform_args.py <image_file> rotate <left|right>")
            return
        direction = sys.argv[3]
        transformed_data = transforms.rotate(img_data, direction)

    elif transformation == "shift":
        if len(sys.argv) != 5:
            print("Usage: transform_args.py <image_file> shift <x_shift> <y_shift>")
            return
        x_shift = int(sys.argv[3])
        y_shift = int(sys.argv[4])
        transformed_data = transforms.shift(img_data, x_shift, y_shift)

    elif transformation == "crop":
        if len(sys.argv) != 7:
            print("Usage: transform_args.py <image_file> crop <x> <y> <width> <height>")
            return
        x = int(sys.argv[3])
        y = int(sys.argv[4])
        width = int(sys.argv[5])
        height = int(sys.argv[6])
        transformed_data = transforms.crop(img_data, x, y, width, height)

    elif transformation == "filter":
        if len(sys.argv) != 6:
            print("Usage: transform_args.py <image_file> filter <r_factor> <g_factor> <b_factor>")
            return
        r_factor = float(sys.argv[3])
        g_factor = float(sys.argv[4])
        b_factor = float(sys.argv[5])
        transformed_data = transforms.filter(img_data, r_factor, g_factor, b_factor)

    elif transformation == "grayscale":
        transformed_data = transforms.grayscale(img_data)

    elif transformation == "mirror":
        transformed_data = transforms.mirror(img_data)

    else:
        print("Unknown transformation:", transformation)
        return

    transformed_img = dict_to_image(transformed_data)
    output_file = image_file.rsplit(".", 1)[0] + "_trans." + image_file.rsplit(".", 1)[1]
    transformed_img.save(output_file)
    print("Transformed image saved as:", output_file)

if __name__ == "__main__": main()



