from transforms import *
import sys
import os

def apply_transforms(input_file, transforms):
    image = load_image(input_file)
    transformed_image = image

    for transform in transforms:
        transform_name = transform[0]
        args = transform[1:]

        if transform_name == 'change_colors':
            original_colors = []
            new_colors = []
            for arg in args:
                if ':' in arg:
                    orig, new = arg.split(':')
                    original_colors.append(tuple(map(int, orig.split(','))))
                    new_colors.append(tuple(map(int, new.split(','))))
                else:
                    original_colors.append(tuple(map(int, arg.split(','))))
                    new_colors.append(tuple(map(int, arg.split(','))))
            transformed_image = change_colors(transformed_image, original_colors, new_colors)

        elif transform_name == 'rotate':
            direction = args[0]
            transformed_image = rotate(transformed_image, direction)

        elif transform_name == 'shift':
            horizontal = int(args[0])
            vertical = int(args[1])
            transformed_image = shift(transformed_image, horizontal, vertical)

        elif transform_name == 'crop':
            x = int(args[0])
            y = int(args[1])
            width = int(args[2])
            height = int(args[3])
            transformed_image = crop(transformed_image, x, y, width, height)

        elif transform_name == 'filter':
            r = float(args[0])
            g = float(args[1])
            b = float(args[2])
            transformed_image = filter(transformed_image, r, g, b)

        elif transform_name == 'mirror':
            transformed_image = mirror(transformed_image)

        elif transform_name == 'grayscale':
            transformed_image = grayscale(transformed_image)

        else:
            print(f"Transform '{transform_name}' not supported.")
            return

    output_file = generate_output_filename(input_file, 'multi')
    save_image(transformed_image, output_file)
    print(f"Transformed image saved as {output_file}")

def generate_output_filename(input_file, suffix):
    base_name, ext = os.path.splitext(input_file)
    return f"{base_name}_trans_{suffix}{ext}"

def main():
    if len(sys.argv) < 3:
        print("Usage: python transform_multi.py <input_file> <transform1> [<args1>...] <transform2> [<args2>...] ...")
        return

    input_file = sys.argv[1]
    transforms = []
    i = 2

    while i < len(sys.argv):
        transform_name = sys.argv[i]
        args = []
        i += 1
        while i < len(sys.argv) and not sys.argv[i].isalpha():
            args.append(sys.argv[i])
            i += 1
        transforms.append([transform_name] + args)

    apply_transforms(input_file, transforms)

if __name__ == '__main__':
    main()
