import sys
from PIL import Image
from transforms import mirror, grayscale, blur

def pil_image_to_dict(image):
    width, height = image.size
    pixels = list(image.getdata())
    pixel_dict = {
        'width': width,
        'height': height,
        'pixels' : pixels
        }
    return pixel_dict


def dict_to_pil_image(data):
    width = data['width']
    height = data['height']
    pixels = data['pixels']
    image = Image.new("RGB", (width, height))
    image.putdata(pixels)
    return image

def main():
    if len(sys.argv) != 3:
        print("Usage: transform_simple.py <image_file> <transform_name>")
        return 1
    image_file = sys.argv[1]
    transform_name = sys.argv[2]

    try:
        image = Image.open(image_file).convert("RGB")
        image_dict = pil_image_to_dict(image)  # Convert PIL Image to dictionary

        if transform_name == "mirror":
            transformed_image_dict = mirror(image_dict)
        elif transform_name == "grayscale":
            transformed_image_dict = grayscale(image_dict)
        elif transform_name == "blur":
            transformed_image_dict = blur(image_dict)
        else:
            raise ValueError(f"Unknown transform name: {transform_name}")

        transformed_image = dict_to_pil_image(transformed_image_dict)
        transformed_image.save('../cafe_trans.jpg')
        return 0
    except FileNotFoundError:
        print(f"Error: Image file '{image_file}' not found.")
        return 2
    except Exception as e:
        print(f"Error applying transformation: {e}")
        return 3

if __name__ == "__main__": main()